var express = require('express');
var router = express.Router();
var _ = require('lodash');
/* GET top active users. */
router.get('/topActiveUsers', function (req, res, next) {
    var page = req.query.page || 1;
    req.model.User.query(function (qb) {
        qb.join('applications', 'users.id', '=', 'applications.user_id');
        qb.whereRaw('applications.created_at BETWEEN NOW()::DATE-EXTRACT(DOW FROM NOW())::INTEGER-7 and NOW()::DATE-EXTRACT(DOW from NOW())::INTEGER');
//        qb.whereBetween('applications.created_at', ['NOW()::DATE-EXTRACT(DOW FROM NOW())::INTEGER-7', 'NOW()::DATE-EXTRACT(DOW from NOW())::INTEGER']);
        qb.select(req.model.knex.raw('COUNT(applications.*) AS count, users.*'));
        qb.groupBy('users.id');
        qb.orderBy('count', 'DESC');
    }).fetchPage({
        pageSize: 2, // Defaults to 10 if not specified
        page: page,
        withRelated: {
            'listings': function (qb) {
                qb.column('name');
                qb.limit(3);
            }
        }
    }).then(function (collection) {
        if(!collection){
            collection ={};
            res.statusCode = 204; //no content
            return ;
        }
        var allowedKeys = ['id', 'name', 'createdAt', 'count'];
        res.send(collection.map(function (attrs) {
            attrs.attributes = _.reduce(attrs.attributes, function (memo, val, key) {
                console.log(key);
                if (key === 'count') {
                    val = parseInt(val);
                }
                if (allowedKeys.indexOf(key) > -1) {
                    memo[_.camelCase(key)] = val;
                }
                return memo;
            }, {});
            attrs.attributes.listings = [];
            attrs.relations.listings.models.map(function (listing) {
                attrs.attributes.listings.push(listing.attributes.name);
            });
            delete attrs.relations.listings;
            return attrs;
        }));
    }).catch(function (err) {
        res.statusCode = 500; //Internal server error - issue with query
        res.send(err);
    });
});

/* GET user Details. */
router.get('/users', function (req, res, next) {
    var user_id = req.query.id;
    req.model.User.where('id', user_id).fetch({ withRelated: {'companies': function (qb) {
    }, 'createdListings': function (qb) {
    }, 'applications.listing': function (qb) {
        qb.columns(['name','id','description']);
        qb.limit(3);
    }} }).then(function (user) {
        if (!user) {
            res.statusCode = 204; //no content
            user = {};
        } else {
            user.relations.companies.models = user.relations.companies.models.map(function (company) {
                company.attributes.isContact = company.pivot.attributes.contact_user;
                delete company.pivot;
                return company;
            });
        }
        res.send(user);
    }).catch(function (err) {
        res.statusCode = 500; //Internal server error - issue with query
        res.send(err);
    });
});

//Render frontend template
router.get('/', function (req, res, next) {
    res.render('index', {});
});


module.exports = router;
