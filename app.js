var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var _ = require('lodash');
var knex = require('knex')({
    client: 'pg',
    debug: true,
    connection: process.env.PG_CONNECTION_STRING || 'postgres://localhost:5432/postgres',
    searchPath: 'knex,public'
});

var bookshelf = require('bookshelf')(knex);
bookshelf.plugin('pagination');
var model = {};
model.knex = knex;
model.helpers = {};
model.helpers.camelCase = function (attrs, allowedKey) {
    return _.reduce(attrs, function (memo, val, key) {
        if (allowedKey.indexOf(key) > -1) {
            memo[_.camelCase(key)] = val;
        }
        return memo;
    }, {});
};
model.User = bookshelf.Model.extend({
    allowedKeys: ['id', 'name', 'created_at', 'companies', 'createdListings', 'applications', 'count'],
    tableName: 'users',
    companies: function () {
        return this.belongsToMany(model.Companies, 'teams', 'user_id', 'company_id').withPivot(['contact_user']);
    },
    createdListings: function () {
        return this.hasMany(model.Listings, 'created_by');
    },
    applications: function () {
        return this.hasMany(model.Applications, 'user_id');
    },
    listings: function () {
        return this.belongsToMany(model.Listings).through(model.Applications).orderBy('applications.created_at', 'desc');
    },
    parse: function (attrs) {
        return model.helpers.camelCase(attrs, this.allowedKeys);
    }
});
model.Companies = bookshelf.Model.extend({
    tableName: 'companies',
    allowedKeys: ['id', 'name', 'created_at', 'contact_user'],
    parse: function (attrs) {
        return model.helpers.camelCase(attrs, this.allowedKeys);
    }
});
model.Team = bookshelf.Model.extend({
    allowedKeys: ['id', 'company_id', 'user_id', 'contact_user'],
    tableName: 'teams',
    parse: function (attrs) {
        return model.helpers.camelCase(attrs, this.allowedKeys);
    }
});
model.Listings = bookshelf.Model.extend({
    tableName: 'listings',
    allowedKeys: ['id', 'name', 'created_at', 'description'],
    applications: function () {
        return this.hasMany(model.Applications);
    },
    listings_users: function () {
        return this.hasOne(model.User);
    },
    parse: function (attrs) {
        return model.helpers.camelCase(attrs, this.allowedKeys);
    }
});

model.Applications = bookshelf.Model.extend({
    tableName: 'applications',
    allowedKeys: ['id', 'name', 'created_at', 'cover_letter'],
    listing: function () {
        return this.hasOne(model.Listings, 'id');
    },
    parse: function (attrs) {
        var currentModel = this;
        return _.reduce(attrs, function (memo, val, key) {
            if (currentModel.allowedKeys.indexOf(key) > -1) {
                memo[_.camelCase(key)] = val;
            }
            return memo;
        }, {});
    }
});

var usersView = require('./routes/users');

var app = express();
// view engine setup
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// Make our db accessible to our router
app.use(function (req, res, next) {
    req.model = model;
    next();
});


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('node-sass-middleware')({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true,
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', usersView);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
