# Skills test for backend and frontend  #

## NPM Packages used 
* express
* knex
* bookshelf
* lodash
* pg


## Database ##
* postgresql

##Commands##
* To install packages : `npm install`
* To Start app : `npm start`
* To setup database config on heroku : `heroku config:add PG_CONNECTION_STRING='postgres://<username>:<password>@<host>:<port>/<dbname>'`


##Endpoints##
* `/` : frontend test
* `/topActiveUsers?page={pageNumber}` : Return top active users with user info, count of applications in the last week and the names of the 3 latest applied listings.
* `/users?id={user.id}` : Returns user info
